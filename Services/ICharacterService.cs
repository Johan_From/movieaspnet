﻿using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;

namespace MovieAPI.Services
{
    // Interface for character service
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        public Task<Character> GetSpecificCharacterAsync(int id);
        public Task<Character> AddCharacterAsync(Character character);
        public Task UpdateCharacterAsync(Character character);
        public Task DeleteCharacterAsync(int id);
        public Task UpdateMovieToCharacterAsync(int id, int[] movieId);
        public bool CharacterExists(int id);
    }
}
