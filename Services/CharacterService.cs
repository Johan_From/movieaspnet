﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;

namespace MovieAPI.Services
{
    public class CharacterService : ICharacterService
    {

        private readonly MovieCharacterDbContext _context;
        public CharacterService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method for getting all the characters
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.Include(x => x.Movies).ToListAsync();
        }

        /// <summary>
        /// Method for getting a specific character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Characters.Where(x => x.CharacterId == id)
                .Include(x => x.Movies)
                .FirstAsync();

        }

        /// <summary>
        /// Method for adding a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Method for updating a charcter
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Boolean method for checking if a charcter exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }

        /// <summary>
        /// Method for deleting a character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method for updating a movie to character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieId"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieToCharacterAsync(int id, int[] movieIds)
        {

            // Gets the character and movie
            var charcterUpdate = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.CharacterId == id)
                .FirstAsync();

            foreach (var movieId in movieIds)
            {
                var movie = await _context.Movies.Where(x => x.MovieId == movieId).FirstAsync();

                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }

                // Add the new movie to characters movies
                charcterUpdate.Movies.Add(movie);
            }

            await _context.SaveChangesAsync();

        }


    }
}