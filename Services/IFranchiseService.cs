﻿using Microsoft.AspNetCore.Mvc;
using MovieAPI.Models.Domain;

namespace MovieAPI.Services
{
    // Interface for franchise service
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public Task UpdateMovieToFranchiseAsync(int id, int[] movieIds);
        public Task<IEnumerable<Franchise>> GetAllMoviesFromFranchiseAsync(int id);
        public Task<IEnumerable<Movie>> GetCharactersFromFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
