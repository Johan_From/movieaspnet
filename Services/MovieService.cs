﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;


namespace MovieAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterDbContext _context;
        public MovieService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method for fecthing all the movies the database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.Include(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Method for adding a movie to the database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Method for updating a movie in the database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method for deleting a movie from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method for getting a specific movie based on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.Where(x => x.MovieId == id)
                .Include(x => x.Characters)
                .FirstAsync();
        }

        /// <summary>
        /// Boolean method for checking if a movie exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        /// <summary>
        /// Method for adding a character to a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateCharactersToMovieAsync(int id, int[] characterIds)
        {
            // Gets the character and the movie

            var movieUpdate = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == id)
                .FirstAsync();

            foreach(var characterId in characterIds)
            {
                var character = await _context.Characters.Where(x => x.CharacterId == characterId).FirstAsync();

                if (character == null)
                {
                    // Throw error
                    throw new KeyNotFoundException();
                }

                // Add and save
                movieUpdate.Characters.Add(character);
            }
            await _context.SaveChangesAsync();

        }

        /// <summary>
        /// Method for getting all the characters from the movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllCharactersFromMovieAsync(int id)
        {
            return await _context.Movies.Where(f => f.MovieId == id).Include(m => m.Characters).ToListAsync();

        }
    }
}
