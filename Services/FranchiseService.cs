﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;

namespace MovieAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharacterDbContext _context;
        public FranchiseService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method for adding a franhise to the database
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Delets an franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Boolean method for checking if franchise exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

        /// <summary>
        /// Method for getting all the franchises
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(m => m.Movies)
                .ToListAsync();

        }

        /// <summary>
        /// Method for getting all the movies from a franchies
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllMoviesFromFranchiseAsync(int id)
        {
            return await _context.Franchises.Where(f => f.FranchiseId == id).Include(m => m.Movies).ToListAsync();

        }

        /// <summary>
        /// Method for getting a specific franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.Where(x => x.FranchiseId == id)
                .Include(x => x.Movies)
                .FirstAsync();
        }

        /// <summary>
        /// Method for updating a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            
        }

        /// <summary>
        /// Method for adding a movie to a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieId"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieToFranchiseAsync(int id, int[] movieIds)
        {
            // gets the franchise and movie
            var franchiseUpdate = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.FranchiseId == id)
                .FirstAsync();

            foreach(var movieId in movieIds)
            {
                var movie = await _context.Movies.FindAsync(movieId);

                if (franchiseUpdate == null || movie == null)
                {
                    throw new KeyNotFoundException();
                }

                // Add the movie
                franchiseUpdate.Movies.Add(movie);

            }

            await _context.SaveChangesAsync();

        }

        /// <summary>
        /// Method for getting all the characters from a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetCharactersFromFranchiseAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).Where(m => m.FranchiseId == id).ToListAsync();
        }
    }
}
