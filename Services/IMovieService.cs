﻿using MovieAPI.Models.Domain;

namespace MovieAPI.Services
{
    // Interface for Movie service
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public Task UpdateCharactersToMovieAsync(int id, int[] movieIds);
        public Task<IEnumerable<Movie>> GetAllCharactersFromMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
