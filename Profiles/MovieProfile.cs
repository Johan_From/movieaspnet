﻿using AutoMapper;
using MovieAPI.Models.DTOs.Movie;
using MovieAPI.Models.Domain;

namespace MovieAPI.Profiles
{
    public class MovieProfile : Profile
    {
        /// <summary>
        /// Mapper profiles for Movies
        /// </summary>
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.CharacterId).ToArray()))
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();
        }
        
    }
}
