﻿using AutoMapper;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Franchise;

namespace MovieAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        // Mapper profiles for franchises
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.MovieId).ToArray()))
                .ReverseMap();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();

        }
    }
}
