﻿using AutoMapper;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;
using System.Linq;


namespace MovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        /// <summary>
        /// Mapper profiles for characters
        /// </summary>
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.MovieId).ToArray()))
                .ReverseMap();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
