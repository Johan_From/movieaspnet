# Movie ASP.NET
An ASP.NET Core Web API that makes it possible to see with swagger movies, characters and francises. The purpose of this project is to use the ASP.NET Core Web API and learn the fundamentals of how to build and create an API with SSMS. The Framework that was being utalized was the Entity Framwork. 

## Table of Contents
1. [General Info](#general-info)
2. [Technologies Used](#technologies-used)
3. [Setup](#setup)
4. [Notes](#notes)
5. [Collaborators](#collaborators)

## General Information
* The app written in C# (.NET 6) and the project is using the template ASP.NET Core Web API.
* The app's purpose is use controllers, DTOs, Domains, Services and much more.
* The app's purpose is to understand APIs and communicate with SQL Server through Entity Framework
* To develop this application, pair programming was being utalized and [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsls-vs).

## Technologies Used
* C# (.NET 6) 
* Entity Framework SQL Server (NuGet package)
* Entity Framework Core (NuGet package)
* Entity Framework Tools (NuGet package)
* Microsoft SQL Server 2019 Express
* Microsoft SQL Server Management Studio 18

## Setup
To get the console application started started follow the steps below:

1. This application was written with Visual Studio 2022 
2. Clone this repository ```https://gitlab.com/Johan_From/movieaspnet.git```
3. Find ```.sln``` file and double click it
4. To run the application press F5 or click the green arrow at the top

## Notes
To see how the database looks and want to mess around with it, you'll need to install the Microsoft SQL Server Management Studio [SSMS](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16) and [SQL Express](https://www.microsoft.com/en-us/Download/details.aspx?id=101064). To get your own database upp and running, follow these steps below after you have installed the above: 

1. Open SSMS
2. Login in with server type ```Database Engine```, server name ```localhost/SQLEXPRESS``` and authentication ```Windows Authentication```
3. Now you are logged in to SSMS

Before running the application, you have to modify the connection. Follow the steps below:
1. In the solution, go find the ```appsettings.json``` 
2. Change the ```DefaultConnection``` server name to ```localhost\\SQLEXPRESS```
3. If that does not work, replace localhost with a ```.``` or the name of your computer (use the command below in cmd)

```
> hostname
```

If this is the first time running the project, then follow these steps below to create the database following pre set migrations: 
1. Type in the Package Manager Console in Visual Studio (View > Other Windows > Package Manger Console):
```
PM> update-database 
```
2. There is a folder named ```Migrations```, but just ignore that folder
3. Run the application, there is an ```Seed``` folder with an ```DbInitializer.cs``` that fills the database with data (if the database is empty it fills it with data, otherwise not)
3. In SSMS, press the button up to the left ↻ (F5 refresh)
4. The tables have now been filled with data
5. You can now interact with Swagger and get data from the API calls

## Collaborators
* [Johan From](https://gitlab.com/Johan_From)
* [Armin Ljajic](https://gitlab.com/ArminExperis)

