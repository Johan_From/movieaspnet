﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;
using MovieAPI.Models.DTOs.Franchise;
using MovieAPI.Models.DTOs.Movie;
using MovieAPI.Services;
using System.Net.Mime;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchiseController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Get all the francises from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>GETSS</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Gets a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            // If frachise is null
            if (franchise == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<FranchiseReadDTO>(franchise));
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            // New franchise to create
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            // Adds it
            var readFranchise = CreatedAtAction("GetFranchise",
                new { id = domainFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));

            return Ok(readFranchise);
        }

        /// <summary>
        /// Updating a franchise in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            // If id not the same
            if (id != dtoFranchise.FranchiseId) return BadRequest();

            // If character exists
            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();

        }

        /// <summary>
        /// Delets a franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            // If franchise not exists
            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            try
            {
                await _franchiseService.DeleteFranchiseAsync(id);
            }
            catch (DbUpdateException ex)
            {

                return BadRequest(ex.Message);  
            }

            return NoContent();

        }

        /// <summary>
        /// Adds a movie to a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        [HttpPut("movies/{franchiseId}")]
        public async Task<IActionResult> AssignMovieToFranchise(int franchiseId, [FromBody] int[] movieIds)
        {
            // If not exists
            if (!_franchiseService.FranchiseExists(franchiseId)) return NotFound();

            // try/catch for updating
            try
            {
                await _franchiseService.UpdateMovieToFranchiseAsync(franchiseId, movieIds);
            }
            catch (KeyNotFoundException)
            {

                return BadRequest("Invalid movie to franchise");
            }

            return NoContent();
        }

        /// <summary>
        /// Gets all the method from a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchiseMovie/{id}")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetAllMoviesFromFranchise(int id)
        {
            // If franchise not exists
            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            // Gets all the movies from franchise
            var franchises = await _franchiseService.GetAllMoviesFromFranchiseAsync(id);

            // For each to get the movies from franchise
            foreach (var franchise in franchises)
            {
                var movies = _mapper.Map<List<MovieReadDTO>>(franchise.Movies);
                return Ok(movies);
            }

            return NotFound();
            
           
        }

        /// <summary>
        /// Gets all the characters from a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchiseCharacter/{id}")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersFromFranchise(int id)
        {
            // If franchise exists
            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            var franchises = await _franchiseService.GetCharactersFromFranchiseAsync(id);

            // For each for getting the charcters frm the franchise
            foreach (var franchise in franchises)
            {
                var movies = _mapper.Map<List<CharacterReadDTO>>(franchise.Characters);
                return Ok(movies);
            }

            return NotFound();


        }

    }
}
