﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;
using MovieAPI.Models.DTOs.Movie;
using MovieAPI.Services;
using System.Net.Mime;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MovieController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Getting all the movies from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());

        }

        /// <summary>
        /// Gets a specific movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            // If movie is null
            if (movie == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MovieReadDTO>(movie));
        }

        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            // Add new movie
            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            var readMovie = CreatedAtAction("GetMovie",
                new { id = domainMovie.MovieId },
                _mapper.Map<MovieReadDTO>(domainMovie));

            return Ok(readMovie);
        }

        /// <summary>
        /// Method for updating a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            // If movie id not the same
            if (id != dtoMovie.MovieId) return BadRequest();

            // If movie exists
            if (!_movieService.MovieExists(id)) return NotFound();

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();

        }

        /// <summary>
        /// Delets a movie from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            // If movie not exists
            if (!_movieService.MovieExists(id)) return NotFound();

            try
            {
                await _movieService.DeleteMovieAsync(id);
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);  
            }
            
            return NoContent();

        }

        /// <summary>
        /// Adds a character to a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        [HttpPut("character/{movieId}")]
        public async Task<IActionResult> UpdateMovieCharacters(int movieId, [FromBody] int[] characterIds)
        {
            // If movie not exists
            if (!_movieService.MovieExists(movieId)) return NotFound();

            // try/catch for adding a charcter to a movie
            try
            {
                await _movieService.UpdateCharactersToMovieAsync(movieId, characterIds);
            }
            catch (KeyNotFoundException)
            {

                return BadRequest("Invalid character");
            }

            return NoContent();
        }

        /// <summary>
        /// Gets all the movies from a franchise
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpGet("movieCharacter/{movieId}")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllMoviesFromFranchise(int movieId)
        {
            // If not exists
            if (!_movieService.MovieExists(movieId)) return NotFound();

            var movies = await _movieService.GetAllCharactersFromMovieAsync(movieId);

            // Foreach for getting the characters from a movie
            foreach (var movie in movies)
            {
                var character = _mapper.Map<List<CharacterReadDTO>>(movie.Characters);
                return Ok(character);
            }

            return NotFound();


        }
    }
}
