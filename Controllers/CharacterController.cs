﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTOs.Character;
using MovieAPI.Services;
using System.Net.Mime;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharacterController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Gets all the character from the database
        /// </summary>
        /// <returns>All the characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());

        }

        /// <summary>
        /// Gets a specific character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            // Gets a character
            Character character = await _characterService.GetSpecificCharacterAsync(id);

            // Checking if character exists
            if (character == null)
            {
                return NotFound();
            }

            // Retunr character
            return Ok(_mapper.Map<CharacterReadDTO>(character));
        }

        /// <summary>
        /// Creates a new character
        /// </summary>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            // New character
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);

            // Add a new character
            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);

            // Add with mapper
            var readCharacter = CreatedAtAction("GetCharacter",
                new { id = domainCharacter.CharacterId },
                _mapper.Map<CharacterReadDTO>(domainCharacter));

            return Ok(readCharacter);
        }

        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            // If id != charcterid that user is searching for
            if(id != dtoCharacter.CharacterId) return BadRequest();

            // If not found
            if (!_characterService.CharacterExists(id)) return NotFound();

            // Add the new character
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();

        }

        /// <summary>
        /// Deletes a character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            // Check if character exists
            if (!_characterService.CharacterExists(id)) return NotFound();

            try
            {
                // Delets a character
                await _characterService.DeleteCharacterAsync(id);
            }
            catch (DbUpdateException ex)
            {

                return BadRequest(ex.Message);
            }
            
            return NoContent();

        }

        /// <summary>
        /// Adds a movie to a character
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        [HttpPut("movies/{characterId}")]
        public async Task<IActionResult> UpdateCharacterMovies(int characterId, [FromBody] int[] movieIds)
        {
            // If character not exists
            if (!_characterService.CharacterExists(characterId)) return NotFound();
            
            // Try/catch for updating
            try
            {
                await _characterService.UpdateMovieToCharacterAsync(characterId, movieIds);
            }
            catch (KeyNotFoundException)
            {

                return BadRequest("Invalid movie");
            }

            return NoContent();
        }
    }
}
