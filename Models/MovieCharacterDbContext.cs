﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models.Domain;

namespace MovieAPI.Models
{
    /// <summary>
    /// DbContext for all objects
    /// </summary>
    public class MovieCharacterDbContext : DbContext
    {
        public MovieCharacterDbContext(DbContextOptions<MovieCharacterDbContext> options) : base(options)
        {
            
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

    }
}
