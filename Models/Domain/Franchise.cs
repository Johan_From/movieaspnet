﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models.Domain
{
    public class Franchise
    {
        public int FranchiseId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public ICollection<Movie>? Movies { get; set; }

    }
}
