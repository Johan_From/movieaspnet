﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models.Domain
{
    public class Character
    {
        public int CharacterId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Alias { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }

        [MaxLength(500)]
        public string Picture { get; set; }

        public ICollection<Movie>? Movies { get; set; }
    }
}
