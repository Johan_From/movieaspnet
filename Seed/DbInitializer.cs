﻿using MovieAPI.Models;
using MovieAPI.Models.Domain;

namespace MovieAPI.Seed
{
    public class DbInitializer
    {
        /// <summary>
        /// Method for filling the database with data from different franchises
        /// </summary>
        /// <param name="context"></param>
        public static void Seed(MovieCharacterDbContext context)
        {
            if (!context.Movies.Any())
            {
                context.Add(new Movie
                {
                    Title = "A new hope",
                    ReleaseYear = 1977,
                    Director = "George Lucas",
                    Genre = "Science Fiction",
                    Picture = "https://www.canvasprintsaustralia.net.au/wp-content/uploads/2014/03/A-New-Hope-Star-Wars-Movie-Poster-768x1034.jpg",
                    Trailer = "https://www.youtube.com/watch?v=eUH2_n8jE70"

                });

                context.Add(new Movie
                {
                    Title = "The Empire Strikes Back",
                    ReleaseYear = 1980,
                    Director = "Irvin Kershner",
                    Genre = "Science Fiction",
                    Picture = "https://wallpaperaccess.com/full/4971210.jpg",
                    Trailer = "https://www.youtube.com/watch?v=JNwNXF9Y6kY"

                });

                context.Add(new Movie
                {
                    Title = "Return of the Jedi",
                    ReleaseYear = 1983,
                    Director = "Richard Marquand",
                    Genre = "Science Fiction",
                    Picture = "https://flxt.tmsimg.com/assets/p8890_p_v8_ae.jpgg",
                    Trailer = "https://www.youtube.com/watch?v=p4vIFhk621QY"

                });

                context.Add(new Movie
                {
                    Title = "The fellowship of the ring",
                    ReleaseYear = 2001,
                    Director = "Peter Jackson",
                    Genre = "Fiction",
                    Picture = "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/63006f8c3192e6f732b9f699f5ed368fb557e6742ca8c01209fef0c046d91113._RI_V_TTW_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=xkiiEjz2O40"

                });

                context.Add(new Movie
                {
                    Title = "The two towers",
                    ReleaseYear = 2002,
                    Director = "Peter Jackson",
                    Genre = "Fiction",
                    Picture = "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/63006f8c3192e6f732b9f699f5ed368fb557e6742ca8c01209fef0c046d91113._RI_V_TTW_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=yAY1HwLXtiI"

                });

                context.Add(new Movie
                {
                    Title = "The return of the king",
                    ReleaseYear = 2003,
                    Director = "Peter Jackson",
                    Genre = "Fiction",
                    Picture = "https://th.bing.com/th/id/R.ce810d8caedf8b2944874bc68794891c?rik=ANfI3u0RxHoenQ&pid=ImgRaw&r=0",
                    Trailer = "https://www.youtube.com/watch?v=Mgjb_qda4Lo"

                });
            }

            if (!context.Franchises.Any())
            {
                context.Add(new Franchise
                {
                    Name = "Star Wars",
                    Description = "A galaxy far far away.",
                });

                context.Add(new Franchise
                {
                    Name = "The lord of the ring",
                    Description = "A story about middle earth where the threat of Saruron and the mighty ring threatens the life of humans, elfs, dwarfs with more."
                });
            }

            if (!context.Characters.Any())
            {
                context.Add(new Character
                {
                    Name = "Luke Skywalker",
                    Alias = "Luke",
                    Gender = "Male",
                    Picture = "https://vignette.wikia.nocookie.net/shaniverse/images/2/2d/Luke.jpg/revision/latest/scale-to-width-down/2000?cb=20200521025141",
                });

                context.Add(new Character
                {
                    Name = "Obi-Wan Kenobi",
                    Alias = "Ben",
                    Gender = "Male",
                    Picture = "https://tse3.mm.bing.net/th/id/OIP.WkY4Z97fa_t6budBf9h_owHaEs?pid=ImgDet&rs=1",
                });

                context.Add(new Character
                {
                    Name = "Darth Vader",
                    Alias = "Darth",
                    Gender = "Male",
                    Picture = "https://yt3.ggpht.com/a/AATXAJw7nkWHnsheQ2sw0U5Xm--p99LA3N0AJTaYdw=s900-c-k-c0xffffffff-no-rj-mo",
                });

                context.Add(new Character
                {
                    Name = "Frodo Baggins",
                    Alias = "Master",
                    Gender = "Male",
                    Picture = "https://yt3.ggpht.com/a/AGF-l788IYws6inqRPerNZujfc8Ig0a_SH8E8huy=s900-c-k-c0xffffffff-no-rj-mo",
                });

                context.Add(new Character
                {
                    Name = "Gandalf",
                    Alias = "Gandalf",
                    Gender = "Male",
                    Picture = "https://th.bing.com/th/id/R.36a7820baecb769a58b596590edd1e67?rik=2njgYG1wiseOTg&pid=ImgRaw&r=0",
                });

                context.Add(new Character
                {
                    Name = "Aragorn",
                    Alias = "King",
                    Gender = "Male",
                    Picture = "https://pm1.narvii.com/6479/9dcfbabbc5002376dc09c90d8f38f18ad80483ca_hq.jpg",
                });
            }

            context.SaveChanges();
        }
    }
}
