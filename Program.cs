using MovieAPI.Models;
using MovieAPI.Seed;

namespace MovieAPI
{
    public class Program
    {
        public static void Main(string[] args)   
        {
            var host = CreateHostBuilder(args).Build();

            // Call seed that fills database with data
            using(var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<MovieCharacterDbContext>();
                DbInitializer.Seed(context);
            }
            host.Run();
            
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

    }
}